<?php
namespace App\Algorithm;

use App\Dao\MatchDao;
use Doctrine\DBAL\DBALException;

class Calculator {
    private $users;
    private $userInput;
    private $matchDao;

    function __construct(MatchDao $matchDao) {
        $this->matchDao = $matchDao;
    }

    /**
     * The main method of this class
     * @param $userInput
     * @param $users
     * @return array
     */
    public function result(array $userInput, array $users) {
        $this->userInput = $userInput;
        $this->users = $users;

        $result = array();
        $ranks = array();

        /* DYNAMIC RANGE */

        // Hourly Rate
        if (isset($this->userInput['hourlyRate'])) {
            array_push($ranks, $this->getTheClosestInteger('Payment', 'hourlyRate'));
        }
        // Age
        if (isset($this->userInput['dateOfBirth'])) {
            array_push($ranks, $this->getTheClosestInteger('Personal', 'dateOfBirth', true));
        }
        // Workload
        if (isset($this->userInput['workload'])) {
            array_push($ranks, $this->getTheClosestInteger('Work', 'workload'));
        }

        /* ALL OR NOTHING */

        // Before
        if (isset($this->userInput['before'])) {
            array_push($ranks, $this->allOrNothing('Payment', 'before', true));
        }
        // After
        if (isset($this->userInput['after'])) {
            array_push($ranks, $this->allOrNothing('Payment', 'after', true));
        }
        // Before | After
        if (isset($this->userInput['before_after'])) {
            array_push($ranks, $this->allOrNothing('Payment', 'before_after', true));
        }
        // After milestone
        if (isset($this->userInput['after_milestone'])) {
            array_push($ranks, $this->allOrNothing('Payment', 'after_milestone', true));
        }
        // City
        if (isset($this->userInput['city'])) {
            array_push($ranks, $this->allOrNothing('Personal', 'city', false));
        }
        // Country
        if (isset($this->userInput['country'])) {
            array_push($ranks, $this->allOrNothing('Personal', 'country', false));
        }
        // Gender
        if (isset($this->userInput['gender'])) {
            array_push($ranks, $this->allOrNothing('Personal', 'gender', false));
        }
        // Highest education
        if (isset($this->userInput['highestEducation'])) {
            array_push($ranks, $this->allOrNothing('Skills', 'highestEducation', false));
        }
        // Project
        if (isset($this->userInput['project'])) {
            array_push($ranks, $this->allOrNothing('TypeOfTask', 'project', true));
        }
        // Feature
        if (isset($this->userInput['feature'])) {
            array_push($ranks, $this->allOrNothing('TypeOfTask', 'feature', true));
        }
        // Bugfix
        if (isset($this->userInput['bugfix'])) {
            array_push($ranks, $this->allOrNothing('TypeOfTask', 'bugfix', true));
        }
        // Other
        if (isset($this->userInput['other'])) {
            array_push($ranks, $this->allOrNothing('TypeOfTask', 'other', true));
        }
        // Milestones
        if (isset($this->userInput['milestones'])) {
            array_push($ranks, $this->allOrNothing('Work', 'milestones', true));
        }
        // Languages
        if (isset($this->userInput['language'])) {
            if (is_array($this->userInput['language'])) {
                array_push($ranks, $this->multipleItems('Languages', 'language'));
            }
            else {
                array_push($ranks, $this->allOrNothing('Languages', 'language', false));
            }
        }
        // Tech
        if (isset($this->userInput['tech'])) {
            array_push($ranks, $this->countVotedItems('technologies', 'tech'));
        }

        // Add up all the user ranks
        foreach ($ranks as $values) {
            foreach ($values as $key => $value) {
                $value = (isset($result[$key])) ? $result[$key] + $value : $value;
                $result[$key] = $value;
            }
        }

        return $result;

    }

    /**
     * Get the closest integer to a specific column
     * @param $table
     * @param $column
     * @param bool $translateTimestamp
     * @return array
     */
    private function getTheClosestInteger(string $table, string $column, bool $translateTimestamp = false) {
        $result = array();

        foreach ($this->users as $user) {
            $query = $this->matchDao->getSingleColumn($table, $column, 'uid', $user);
            if (!empty($query)) {
                $dbValue = ($query[0][$column] !== null) ? ($translateTimestamp) ? date('Y', $query[0][$column]) : $query[0][$column] : 0;
                $userInput = ($translateTimestamp) ? date('Y', $this->userInput[$column]) : $this->userInput[$column];

                /* ALGORITHM */

                // Formula: if a >= b: 100 - (a - b)
                if ($dbValue >= $userInput) {
                    $range = 100 - ($dbValue - $userInput);
                    $range = ($column == 'workload') ? 100 : $range; // The range of 'workload' needs to be 100
                } // Formula: if a >= b: 100 - (b - a)
                else {
                    $range = 100 - ($userInput - $dbValue);
                }
                $range = ($range < 0) ? 0 : $range; // The range should not be less than 0
                $result[$user] = $range;
            }
        }
        return $result;
    }

    /** If there is a match, the range will be 100 - nothing more or less
     * @param $table
     * @param $column
     * @param $boolean
     * @return array
     */
    private function allOrNothing(string $table, string $column, bool $boolean) {
        $result = array();

        foreach ($this->users as $user) {
            if ($boolean) { // The value is either 0 or 1
                $query = $this->matchDao->getSingleColumn($table, $column, 'uid', $user);
                if (isset($query[0][$column]) && $query[0][$column] == 1) {
                    $result[$user] = 100;
                }
            }
            else { // The value is a string, which should match
                $query = $this->matchDao->getColumnByUid($table, $column, $column, $this->userInput[$column], $user);
                if (!empty($query)) {
                    $result[$user] = 100;
                }
            }
        }
        return $result;
    }

    /**
     * Get the range of multiple items from an array
     * @param $table
     * @param $column
     * @return array
     */
    private function multipleItems(string $table, string $column) {
        $result = array();

        foreach ($this->users as $user) {
            foreach ($this->userInput[$column] as $value) {
                $query = $this->matchDao->getSingleColumn($table, $column, $column, $value);
                if ($query) {
                    $result[$user] = 100;
                }
            }
        }
        return $result;
    }

    /**
     * Count items of a collection based on votes
     * @param string $table
     * @param string $column
     * @return array
     */
    private function countVotedItems(string $table, string $column) {
        $result = array();
        $countedItems = count($this->userInput[$column]);

        // Go through each user
        foreach ($this->users as $user) {
            $subtotal = array();
            // Go through each item
            foreach ($this->userInput[$column] as $item) {
                // Get the query
                $query = null;
                try {
                    $query = $this->matchDao->rawSQL(/** @lang text */ "SELECT uid, tech, vote FROM $table WHERE tech = '$item' AND uid = '$user'");
                }
                catch (DBALException $e) {
                    // var_dump($e);
                }
                // If the query could be found, add it to subtotal
                if ($query) {
                    array_push($subtotal, 1);
                }
                else {
                    array_push($subtotal, 0);
                }
            }

            // Find out how many matches they are
            $match = $countedItems;
            foreach ($subtotal as $value) {
                if ($value == 0) {
                    $match--;
                }
            }
            // Formula: (founded matches / all existing items) * 100
            // var_dump($countedItems);
            $result[$user] = ($match != 0) ? ($match / $countedItems) * 100 : 0;
        }
        return $result;
    }
}
