<?php
namespace App\Algorithm;


use App\Dao\MatchDao;
use Doctrine\DBAL\DBALException;

class Collection {
    private $matchDao;
    private $data;
    public $users = array();

    public function __construct(MatchDao $matchDao) {
        $this->matchDao = $matchDao;
    }

    public function getAllUsers($data) {
        $this->data = $data;

        /* GET THE 100% MATCH */

        // Personal: City
        $this->addUser('Personal', 'city');
        // Personal: Country
        $this->addUser('Personal','country');
        // Personal: Gender
        $this->addUser('Personal','gender');
        // Languages: Language
        $this->addUser('Languages','language');
        // Payment: before
        $this->addUser('Payment','before');
        // Payment: after
        $this->addUser('Payment','after');
        // Payment: before_after
        $this->addUser('Payment','before_after');
        // Payment: after_milestone
        $this->addUser('Payment','after_milestone');
        // Skills: highest_education
        $this->addUser('Skills','highestEducation');
        // Technologies: tech
        $this->addUser('Technologies','tech');
        // Work: milestones
        $this->addUser('Work','milestones');
        // TypeOfTask: project
        $this->addUser('TypeOfTask','project');
        // TypeOfTask: feature
        $this->addUser('TypeOfTask','feature');
        // TypeOfTask: bugfix
        $this->addUser('TypeOfTask','bugfix');
        // TypeOfTask: other
        $this->addUser('TypeOfTask','other');

        /* GET THE CLOSEST VALUES */
        try {
            // Personal: dateOfBirth
            // Frontend Note: Translate the date to timestamp using: (new Date("1985")).getTime() / 1000;
            $this->addTheClosestUser('personal', 'dateOfBirth', 'date_of_birth');
            // Payment: hourlyRate
            $this->addTheClosestUser('payment', 'hourlyRate', 'hourly_rate');
            // Work: workload
            $this->addTheClosestUser('work', 'workload');
        }
        catch (DBALException $e) {
            var_dump($e);
        }

        return $this->users;
    }

    /**
     * Add a user if the value 100% matches the database
     * @param $table
     * @param $column
     */
    private function addUser($table, $column) {
        if (isset($this->data[$column])) {
            $this->users = $this->matchDao->findAGeneralMatch($table, $column, $this->data[$column], $this->users);
        }
    }

    /**
     * Add the closest user. Example: User types in 50. Get 2 of the closest values, which could be for instance 40 or 60.
     * @param $table
     * @param $symfonyColumn
     * @param null $sqlColumn
     * @throws DBALException
     */
    private function addTheClosestUser($table, $symfonyColumn, $sqlColumn = null) {
        $sqlColumn = ($sqlColumn == null) ? $symfonyColumn : $sqlColumn;
        if (isset($this->data[$symfonyColumn])) {
            $val = $this->data[$symfonyColumn];
            $query = $this->matchDao->rawSQL( /** @lang text */ "SELECT uid FROM $table WHERE $sqlColumn IS NOT NULL ORDER BY ABS($sqlColumn - $val) LIMIT 10");
            if (!empty($query)) {
                foreach ($query as $item) {
                    foreach ($item as $value) {
                        if (!in_array($value, $this->users)) {
                            array_push($this->users, $value);
                        }
                    }
                }
            }
        }
    }
}
