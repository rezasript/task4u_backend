<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AutofillController extends AbstractController {
    private $autofillService;

    public function __construct(\App\Service\AutofillService $autofillService) {
        header("Access-Control-Allow-Origin: *");
        $this->autofillService = $autofillService;
    }

    /**
     * @Route("/autofill/save", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function save(Request $request) : Response {
        $data = json_decode($request->getContent(), true);
        $this->autofillService->save($data);
        return new Response("x");
    }

    /**
     * @Route("/autofill/has-data", methods={"GET"})
     * @return Response
     */
    public function hasData() {
        $hasEnoughData = ($this->autofillService->hasEnoughData()) ? 1 : 0;
        return new Response($hasEnoughData);
    }

    /**
     * @Route("/autofill/get-data", methods={"GET"})
     * @return Response
     */
    public function getData() {
        $data =  json_encode($this->autofillService->getData(20), JSON_UNESCAPED_SLASHES) ?? null;
        return new Response($data);
    }
}
