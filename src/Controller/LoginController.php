<?php
namespace App\Controller;

use App\Service\LoginService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController {
    public $loginService;
    public function __construct(LoginService $loginService) {
        header("Access-Control-Allow-Origin: *");
        $this->loginService = $loginService;
    }

    /**
     * @Route("/login", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function login(Request $request) {
        $data = json_decode($request->getContent(), true);
        $isLoggedIn = $this->loginService->login($data);
        return ($isLoggedIn) ? new Response(1) : new Response(0);
    }

    /**
     * @Route("/login/get-cookie", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function getCookie(Request $request) {
        $cookie = json_encode($this->loginService->getCookie($request->getContent()));
        return new Response($cookie);
    }

    /**
     * @Route("/login/get-cookie-by-uid", methods={"GET"})
     * @return Response
     */
    public function getCookieByUid() {
        $cookie = $this->loginService->getCookieByUid();
        return new Response($cookie);
    }
}
