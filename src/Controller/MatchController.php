<?php
namespace App\Controller;

use App\Service\MatchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class matchController
 * @package App\Controller
 */
class MatchController extends AbstractController {
    private $matchService;

    public function __construct(MatchService $matchService) {
        header("Access-Control-Allow-Origin: *");
        $this->matchService = $matchService;
    }

    /**
     * @Route("/match", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function find(Request $request) {
        $data = json_decode($request->getContent(), true);
        $output = json_encode($this->matchService->getMatch($data));
        return new Response($output);
    }
}
