<?php
namespace App\Controller;

use App\Service\ProfileService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController {
    private $profileService;

    /**
     * ProfileController constructor.
     * @param ProfileService $profileService
     */
    function __construct(ProfileService $profileService) {
        header("Access-Control-Allow-Origin: *");
        $this->profileService = $profileService;
    }

    /**
     * Updates an entity
     * @Route("/profile/update", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function update(Request $request) {
        $data = json_decode($request->getContent(), true);
        $data = $this->profileService->update($data);
        return new Response($data);
    }

    /**
     * Returns all profile details
     * @Route("/profile/output", methods={"GET"})
     * @return Response
     */
    public function output() {
        $data = json_encode($this->profileService->output());
        return new Response($data);
    }

    /**
     * Returns all profile details
     * @Route("/profile/profile-picture", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function setImage(Request $request) {
        $data = json_encode($this->profileService->setImage($request));
        return new Response($data);
    }

}
