<?php

namespace App\Controller;

use App\Service\TaskService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends AbstractController {
    private $taskService;
    public function __construct(TaskService $taskService) {
        header("Access-Control-Allow-Origin: *");
        $this->taskService = $taskService;
    }

    /**
     * @Route("/send-task", methods={"POST"})
     * @param Request $request
     * @return string
     */
    public function send(Request $request) {
        $request = json_decode($request->getContent(), true);
        $service = $this->taskService->validateToSendEmail($request);
        return new Response($service);
    }
}
