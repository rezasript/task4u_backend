<?php
namespace App\Controller;

use App\Service\TechnologyService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TechnologyController extends AbstractController {
    private $technologyService;

    public function __construct(TechnologyService $technologyService) {
        header("Access-Control-Allow-Origin: *");
        $this->technologyService = $technologyService;
    }

    /**
     * Get the official technologies
     * @Route("technologies", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function getTechnologies(Request $request) {
        $technologies = $this->technologyService->getTechnologies($request->getContent());
        return new Response(json_encode($technologies));
    }
}
