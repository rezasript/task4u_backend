<?php
namespace App\Dao;

use App\Entity\FormInputs;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AutofillDao extends AbstractController {
    /**
     * Returns array of user inputs
     * @return FormInputs[]
     */
    public function getData() {
        $uid = $this->get('session')->get('id');
        return $this->getDoctrine()->getRepository(FormInputs::class)->
        findBy(
            array('uid' => $uid),
            array('id' => 'DESC'),
            100
        );


    }

    /**
     * All form inputs will be stored as JSON
     * @param array $data
     */
    public function save(array $data): void {
        $formInputs = new FormInputs();
        $formInputs->setUid($this->get('session')->get('id'));
        $formInputs->setData($data);
        $formInputs->setTimestamp(time());
        $this->getDoctrine()->getManager()->persist($formInputs);
        $this->getDoctrine()->getManager()->flush();
    }
}
