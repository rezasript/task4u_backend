<?php
namespace App\Dao;

use App\Entity\Cookies;
use App\Entity\Users;
use App\Repository\UsersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LoginDao extends AbstractController {

    function __construct(UsersRepository $usersRepository) {
    }

    /**
     * @param $data
     * @return bool
     */
    public function login($data) {

        // Get the hash from the password
        $passwordIsSet = !empty($this->getDoctrine()->getRepository(Users::class)->getHash($data['email'])[0]['password']);

        if ($passwordIsSet) {
            // Use hash instead of the basic password
            $hashedPassword = $this->getDoctrine()->getRepository(Users::class)->getHash($data['email'])[0]['password'];

            // Get the query
            $result = $this->getDoctrine()
                ->getRepository(Users::class)
                ->getLoginAuth($data, $hashedPassword);

            return ($result) ? true : false;

        }
        else {
            return false;
        }
    }

    // Get the users ID
    /**
     * @param $email
     * @return mixed
     */
    public function getId($email) {
        return $this->getDoctrine()->getRepository(Users::class)->getId($email)[0]['id'];
    }


    /**
     * Set a cookie in the database
     * @return void
     */
    public function setCookie() {
        // Generate a random key for the login cookie
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $loginKey = substr(str_shuffle($chars), 0, 62);
        $entityManager = $this->getDoctrine()->getManager();
        $cookies = $entityManager->getRepository(Cookies::class)->findOneBy(array('uid' => $this->get('session')->get('id')));
        // Store the cookie, if it does not exist
        if (!$cookies || !$cookies->getLogin()) {
            $cookies = new Cookies();
            $cookies->setUid($this->get('session')->get('id'));
            $cookies->setLogin($loginKey);
        }
        else {
            $cookies->setLogin($loginKey);
        }
        $entityManager->persist($cookies);
        $entityManager->flush();
    }
}
