<?php
namespace App\Dao;

use App\Entity\Personal;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MatchDao extends AbstractController {
    private $entityManager;
    public $users;

    /**
     * MatchDao constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * Finds a 100% match
     * @param $table
     * @param $column
     * @param $value
     * @param $users
     * @return array
     */
    public function findAGeneralMatch($table, $column, $value, $users) {
        $this->users = $users;

        // Get the query
        $getQuery = function($table, $column, $value) {
            $prefix = strtolower($table[0]);
            $query = $this->getDoctrine()->getRepository('App\Entity\\' . $table)->data("$prefix.uid, $prefix.$column", $column, $value);
            // Add multiple users
            if (!empty($query)) {
                foreach ($query as $data) {
                    // Get UID
                    if (!in_array($data['uid'], $this->users)) {
                        array_push($this->users, $data['uid']);
                    }
                }
            }
        };
        // Loop if the value is an array
        if (is_array($value)) {
            foreach ($value as $item) {
                $getQuery($table, $column, $item);
            }
        }
        // The value is not an array
        else {
            $getQuery($table, $column, $value);
        }

        return $this->users;
    }

    /**
     * Use a simple raw SQL
     * @param $sql
     * @return mixed[]
     * @throws DBALException
     */
    public function rawSQL($sql) {
        $statement = $this->entityManager->getConnection()->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }

    /**
     * This will simply return the value of a single column
     * @param $table
     * @param $select
     * @param $where
     * @param $value
     * @return mixed
     */
    public function getSingleColumn($table, $select, $where, $value) {
        $prefix = strtolower($table[0]);
        return $this->getDoctrine()->getRepository('App\Entity\\' . $table)->data("$prefix.$select", $where, $value);
    }

    public function getColumnByUid($table, $select, $where, $value, $uid) {
        return $this->getDoctrine()->getRepository(Personal::class)->columnByUid($table, $select, $where, $value, $uid);
    }

    /**
     * Get the dynamic info of a developer, who has been selected
     * @param $users
     * @param $data
     * @return mixed
     */
    public function getDeveloperInfo($users, $data) {
        $tables = array('users');
        $selection = 'personal.uid, personal.first_name, personal.last_name, personal.date_of_birth, personal.city, personal.country, personal.profile_image, ';

        foreach ($data as $key => $value) {
            // Personal
            if ($key == 'country' || $key == 'city' || $key == 'gender' || $key == 'dateOfBirth') {
                $key = ($key == 'dateOfBirth') ? 'date_of_birth' : $key;
                array_push($tables, 'personal');
                $selection = $selection . 'personal.' . $key . ', ';
            }
            // Payment
            if ($key == 'before' || $key == 'after' || $key == 'before_after' || $key == 'after_milestone' || $key == 'hourlyRate') {
                $key = ($key == 'hourlyRate') ? 'hourly_rate' : $key;
                array_push($tables, 'payment');
                $selection = $selection . 'payment.' . $key . ', ';
            }
            // Languages
            if ($key == 'language') {
                array_push($tables,'languages');
                $selection = $selection . 'GROUP_CONCAT(DISTINCT languages.language) AS languages' . ', ';
            }
            // Skills
            if ($key == 'highestEducation') {
                $key = ($key == 'highestEducation') ? 'highest_education' : $key;
                array_push($tables, 'skills');
                $selection = $selection .'skills.'. $key . ', ';
            }
            // Technologies
            if ($key == 'tech') {
                array_push($tables,'technologies');
                $selection = $selection . 'GROUP_CONCAT(DISTINCT technologies.tech) AS technologies' . ', ';
            }
            // Type of task
            if ($key == 'project' || $key == 'feature' || $key == 'bugfix' || $key == 'other') {
                array_push($tables,'type_of_task');
                $selection = $selection .'type_of_task.'. $key . ', ';
            }
            // Work
            if ($key == 'workload' || $key == 'milestones') {
                array_push($tables, 'work');
                $selection = $selection .'work.'. $key . ', ';
            }
        }
        $selection = substr_replace($selection, '', -2);

        // Return all user info
        $usersWithInfo = array();
        foreach ($users as $key => $value) {
            if ($key) {
                $usersWithInfo[$key] = array();

                // Let's write the SQL as a string
                $rawSql = "SELECT $selection FROM personal ";
                $registeredTable = array();
                foreach ($tables as $tableName) {
                    if (!in_array($tableName, $registeredTable) && $tableName != 'personal') {
                        if ($tableName === 'users') {
                            $rawSql = $rawSql . "LEFT JOIN $tableName ON $tableName.id = personal.uid ";
                        } else {
                            $rawSql = $rawSql . "LEFT JOIN $tableName ON $tableName.uid = personal.uid ";
                        }
                        array_push($registeredTable, $tableName);
                    }
                }
                // Hint: Use echo to view the query
                $rawSql = $rawSql . "WHERE personal.uid = $key";

                // Create the final array
                try {
                    $usersWithInfo[$key]['data'] = $this->rawSQL($rawSql);
                } catch (DBALException $e) {
                    var_dump($e->getMessage());
                }

                // Divide the rank of a user by outputted data and subtract it by static outputted data (8) like "UID", "first_name", "last_name", "date_of_birth", "city", "country", "profile_image"
                $availableData = 8;
                // Reduce $availableData by available important fields
                foreach ($data as $columnKey => $columnValue) {
                    $columnKey = ($columnKey === 'dateOfBirth') ? 'date_of_birth' : $columnKey;
                    if ($columnKey === 'gender') {
                        $availableData++;
                    }
                    if (isset($usersWithInfo[$key]['data'][0]) && array_key_exists($columnKey, $usersWithInfo[$key]['data'][0])) {
                        $availableData--;
                    }
                }
                if (isset($usersWithInfo[$key]['data'][0]) && count($usersWithInfo[$key]['data'][0]) === $availableData) { // No need to divide it by 0, because of error
                    $relative = $users[$key];
                } else {
                    $relative = ($usersWithInfo[$key]['data']) ? $users[$key] / (count($usersWithInfo[$key]['data'][0]) - $availableData) : null;
                }

                $usersWithInfo[$key]['rank']['relative'] = ($relative > 100) ? 100 : $relative;
                $usersWithInfo[$key]['rank']['absolute'] = $users[$key];
            }
        }

        return $usersWithInfo;

    }

    /**
     * Filter the user list by mandatory columns
     * @param array $users
     * @param array $columns
     * @param array $values
     * @return array
     * @throws Exception
     */
    public function filterUsers(array $users, array $columns, array $values) {
        $result = array();
        foreach ($users as $user) {
            $query = $this->getDoctrine()->getRepository(Personal::class)->matchWithMandatoryEntities('p.uid', 'uid', $user, $columns, $values);
            if (!empty($query)) {
                array_push($result, $query[0]['uid']);
            }
        }

        return $result;
    }

}
