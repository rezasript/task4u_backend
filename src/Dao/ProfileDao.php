<?php
namespace App\Dao;

use App\Entity\Languages;
use App\Entity\Payment;
use App\Entity\Personal;
use App\Entity\Skills;
use App\Entity\Technologies;
use App\Entity\TechnologiesOfficial;
use App\Entity\TypeOfTask;
use App\Entity\Work;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ProfileDao extends AbstractController {
    private $id;
    private $em;

    function __construct(EntityManagerInterface $em, SessionInterface $session) {
        $this->id = $session->get('id');
        $this->em = $em;
    }

    // Update the table 'personal'
    /**
     * @param $column
     * @param $value
     */
    public function updatePersonal($column, $value) {
        $setColumn = "set{$column}";

        $search = $this->getDoctrine()->getRepository(Personal::class)->findBy(array("uid" => $this->id));

        // Update a value with an existing UID
        if (count($search) == 0) {
            $personal = new Personal();
            $personal->setUid($this->id);
        }
        // Insert a new row
        else {
            $personal = $search[0];
        }
        $personal->$setColumn($value);

        $this->em->persist($personal);
        $this->em->flush();
    }

    /**
     * Update the table 'languages'
     * @param $type
     * @param $value
     * @return int
     */
    public function updateLanguage($type, $value) {
        $result = 0;
        if ($type == 'skill') {
            $language = $this->getDoctrine()->getRepository(Languages::class)->findBy(array('uid' => $this->id));
            if (count($language) == 0) {
                $language = new Languages();
            }
            else {
                $language = $language[0];
            }
            $language->setSkillId($this->get('session')->get('id'));
            $language->setUid($this->id);
            $language->setLanguage($value);
            $this->em->persist($language);
            $this->em->flush();
            $result = 1;
        }
        return $result;
    }

    public function updateTypeOfTask($column, $value) {
        $typeOfTask = $this->getDoctrine()->getRepository(TypeOfTask::class)->findBy(array('uid' => $this->id));
        if (count($typeOfTask) == 0) {
            $typeOfTask = new TypeOfTask();
        }
        else {
            $typeOfTask = $typeOfTask[0];
        }
        $typeOfTask->setUid($this->id);
        $methodName = 'set' . ucfirst($column);
        $typeOfTask->$methodName($value);
        $this->em->persist($typeOfTask);
        $this->em->flush();
        return 1;
    }

    /**
     * Update the payment data
     * @param $data
     * @return int
     */
    public function updatePayment($data) {
        if (!isset($data['column']) || !isset($data['value'])) return 0;
        $result = 0;
        $type = null;
        $payment = new Payment();

        if ($data['type'] == 'skill') {
            $workId = $this->getDoctrine()->getRepository(Work::class)->findBy(array('uid' => $this->id))[0]->getId();
            $activePayment = $this->getDoctrine()->getRepository(Payment::class)->findBy(array('workId' => $workId));
            $payment = (!empty($activePayment)) ? $activePayment[0] : $payment;
        }

        if ($data['column'] == 'payday') {
            $payment->setUid($this->id);
            // Payment is 'before'
            if ($data['value'] == 'before') {
                $payment->setBefore(1);
                $payment->setAfter(0);
                $payment->setBeforeAfter(0);
                $payment->setAfterMilestone(0);
                $result = 1;
            }
            // Payment is 'after'
            elseif ($data['value'] == 'after') {
                $payment->setBefore(0);
                $payment->setAfter(1);
                $payment->setBeforeAfter(0);
                $payment->setAfterMilestone(0);
                $result = 1;
            } // Payment is 'before_after'
            elseif ($data['value'] == 'beforeAfter') {
                $payment->setBefore(0);
                $payment->setAfter(0);
                $payment->setAfterMilestone(0);
                $payment->setBeforeAfter(1);
                $result = 1;
            } // Payment is 'after_milestone'
            elseif ($data['value'] == 'afterMilestone') {
                $payment->setBefore(0);
                $payment->setAfter(0);
                $payment->setBeforeAfter(0);
                $payment->setAfterMilestone(1);
                $result = 1;
            }
        }

        // Get the skill or task ID
        if (isset($data['type'])) {
            if ($data['type'] == 'skill') {
                $id = $this->getDoctrine()->getRepository(Work::class)->findBy(array('uid' => $this->id));
                $type = $id[0]->getId();
                $result = 1;
            }
            elseif ($data['type'] == 'task') {
                // Todo: Add the Task ID
                $result = 1;
            }
        }

        // Hourly rate
        if (isset($data['column'])) {
            // Hourly Rate
            if ($data['column'] == 'hourlyRate') {
                $payment->setUid($this->id);
                $payment->setHourlyRate($data['value']);
                $result = 1;
            }

            // Milestones
            if ($data['column'] == 'milestones') {
                $payment->setMilestones($data['value']);
                $result = 1;
            }

            if ($result == 1) {
                $payment->setWorkId($type);
                $this->em->persist($payment);
                $this->em->flush();
            }
        }
        return $result;
    }

    /**
     * Update the skill table
     * @param $data
     * @return int
     */
    public function updateSkill($data) {
        // Update the highest education level
        $activeUser = $this->getDoctrine()->getRepository(Skills::class)->findBy(array('uid' => $this->id));
        $skills = (!empty($activeUser)) ? $activeUser[0] : new Skills();
        $skills->setHighestEducation($data['value']);
        $this->em->persist($skills);
        $this->em->flush();
        return 1;
    }

    /**
     * Deletes all unnecessary values after an update
     * @param array $values The values which should NOT be deleted
     */
    private function _deleteTech(array $values) {
        $techList = $this->getDoctrine()->getRepository(Technologies::class)->findBy(array('uid' => $this->id));
        foreach ($techList as $tech) {
            if (!in_array($tech->getTech(), $values)) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository(Technologies::class)->findOneBy(array('uid' => $this->id, 'tech' => $tech->getTech()));
                $em->remove($entity);
                $em->flush();
            }
        }
    }

    /**
     * Update the technologies table
     * @param string $value Value that should be inserted
     * @param array $values Values which should be deleted
     * @return int
     */
    public function updateTech(string $value, array $values): int {
        // Delete technologies
        $this->_deleteTech($values);
        // Insert a technology
        if (!empty($value)) {
            $tech = new Technologies();
            $techExists = $this->getDoctrine()->getRepository(Technologies::class)->findOneBy(array('uid' => $this->id, 'tech' => $value));
            if (empty($techExists)) {
                $tech->setSkillId($this->id);
                $tech->setUid($this->id);
                $tech->setTech($value);
                $tech->setVote(0);
                $this->em->persist($tech);
                $this->em->flush();
            }

            // Update the table "technologies_official"
            $technologiesOfficial = new TechnologiesOfficial();
            $technology = $this->getDoctrine()->getRepository(TechnologiesOfficial::class)->findOneBy(array('technology' => $value));

            // Create a new row
            if (!$technology) {
                $technologiesOfficial->setTechnology($value);
                $technologiesOfficial->setCounter(1);
                $object = $technologiesOfficial;
            } else { // Update the row
                $technology->setCounter($technology->getCounter() + 1);
                $object = $technology;
            }
            $this->em->persist($object);
            $this->em->flush();
        }

        return 1;
    }

    /**
     * Update the work table
     * @param array $data
     * @return int
     */
    public function updateWork(array $data): int {
        $setColumn = "set{$data['column']}";
        $activeUser = $this->getDoctrine()->getRepository(Work::class)->findBy(array('uid' => $this->id));
        $work = (!empty($activeUser)) ? $activeUser[0] : new Work();
        $work->setUid($this->id);
        $work->$setColumn($data['value']);
        $this->em->persist($work);
        $this->em->flush();
        return 1;
    }

    /**
     * Outputs all data
     * @return array
     */
    public function output(): array {
        $technologyList = array();

        $personal = $this->getDoctrine()->getRepository(Personal::class)->findOneBy(array('uid' => $this->id));
        $languages = $this->getDoctrine()->getRepository(Languages::class)->findOneBy(array('uid' => $this->id));
        $skill = $this->getDoctrine()->getRepository(Skills::class)->findOneBy(array('uid' => $this->id));
        $work = $this->getDoctrine()->getRepository(Work::class)->findOneBy(array('uid' => $this->id));
        $typeOfTask = $this->getDoctrine()->getRepository(TypeOfTask::class)->findOneBy(array('uid' => $this->id));
        $payment = $this->getDoctrine()->getRepository(Payment::class)->findOneBy(array('uid' => $this->id));
        $technologies = $this->getDoctrine()->getRepository(Technologies::class)->findBy(array('uid' => $this->id));

        // Get the payday
        $payday = null;
        if ($payment) {
            if ($payment->getBefore() == 1) {
                $payday = 'before';
            } elseif ($payment->getAfter() == 1) {
                $payday = 'after';
            } elseif ($payment->getBeforeAfter() == 1) {
                $payday = 'beforeAfter';
            } elseif ($payment->getAfterMilestone() == 1) {
                $payday = 'afterMilestone';
            }
        }

        // Get the technologies
        foreach ($technologies as $technology) {
            array_push($technologyList, $technology->getTech());
        }


        return array(
            'gender'           => $personal ? $personal->getGender() : null,
            'firstName'        => $personal ? $personal->getFirstName() : null,
            'lastName'         => $personal ? $personal->getLastName() : null,
            'role'             => $personal ? $personal->getRole() : null,
            'city'             => $personal ? $personal->getCity() : null,
            'country'          => $personal ? $personal->getCountry() : null,
            'dateOfBirth'      => $personal ? $personal->getDateOfBirth() : null,
            'profileImage'     => $personal ? $personal->getProfileImage() : null,
            'description'      => $personal ? $personal->getDescription() : null,
            'language'         => $languages ? $languages->getLanguage() : null,
            'highestEducation' => $skill ? $skill->getHighestEducation() : null,
            'workload'         => $work ? $work->getWorkload() : null,
            'milestones'       => $work ? $work->getMilestones() : null,
            'project'          => $typeOfTask ? $typeOfTask->getProject() : null,
            'feature'          => $typeOfTask ? $typeOfTask->getFeature() : null,
            'bugfix'           => $typeOfTask ? $typeOfTask->getBugfix() : null,
            'other'            => $typeOfTask ? $typeOfTask->getOther() : null,
            'hourlyRate'       => $payment ? $payment->getHourlyRate() : null,
            'payday'           => $payday ?? null,
            'technologies'     => $technologyList ?? null
        );
    }

    /**
     * Save the filename in database
     * @param $filename
     */
    public function setImage($filename) {
        $personal =  $this->getDoctrine()->getRepository(Personal::class)->findOneBy(array('uid' => $this->id));
        $personal->setProfileImage($filename);
        $this->em->persist($personal);
        $this->em->flush();
    }
}
