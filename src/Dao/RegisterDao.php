<?php
namespace App\Dao;

use App\Entity\Languages;
use App\Entity\Payment;
use App\Entity\Personal;
use App\Entity\Skills;
use App\Entity\TypeOfTask;
use App\Entity\Users;

use App\Entity\Work;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RegisterDao extends AbstractController {
    function register($data) {
        $entityManager = $this->getDoctrine()->getManager();

        // User
        $user = new Users();
        $user->setEmail($data['email']);
        $user->setActive(false);
        $user->setPassword(password_hash($data['password'], PASSWORD_DEFAULT));
        $user->setRegisterDate(time());
        $entityManager->persist($user);
        $entityManager->flush();

        // Personal
        $personal = new Personal();
        $personal->setUid($user->getId());
        $entityManager->persist($personal);

        // Languages
        $languages = new Languages();
        $languages->setUid($user->getId());
        $entityManager->persist($languages);

        // Skills
        $skill = new Skills();
        $skill->setUid($user->getId());
        $entityManager->persist($skill);

        // Work
        $work = new Work();
        $work->setUid($user->getId());
        $entityManager->persist($work);

        // Payment
        $payment = new Payment();
        $payment->setWorkId($user->getId());
        $payment->setUid($user->getId());
        $entityManager->persist($payment);

        // Type of task
        $typeOfTask = new TypeOfTask();
        $typeOfTask->setUid($user->getId());
        $typeOfTask->setProject(1);
        $typeOfTask->setFeature(1);
        $typeOfTask->setBugfix(1);
        $typeOfTask->setOther(1);
        $entityManager->persist($typeOfTask);

        $entityManager->flush();
    }
}
