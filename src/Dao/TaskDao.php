<?php
namespace App\Dao;

use App\Entity\Personal;
use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TaskDao extends AbstractController {
    /**
     * Get the needed data from sender
     * @param $id
     * @return array|int
     */
    public function getUserData($id) {
        $sender = array();
        foreach ($this->getDoctrine()->getRepository(Personal::class)->findBy(array('uid' => $id)) as $object) {
            $sender['firstName'] = $object->getFirstName();
            $sender['lastName'] = $object->getLastName();
            $sender['city'] = $object->getCity();
            $sender['country'] = $object->getCountry();
            $sender['phone'] = $object->getPhone();
            $sender['email'] = $this->get('session')->get('user');
            return $sender;
        }
        return 0;
    }

    /**
     * Get receivers email address by UID
     * @param int $id
     * @return string
     */
    public function getReceiversEmail(int $id) {
        return $this->getDoctrine()->getRepository(Users::class)->findOneBy(array('id' => $id))->getEmail();
    }
}
