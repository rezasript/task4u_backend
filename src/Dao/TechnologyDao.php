<?php
namespace App\Dao;

use App\Entity\TechnologiesOfficial;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class TechnologyDao
 * @package App\Dao
 */
class TechnologyDao extends AbstractController {
    /**
     * Returns an array with the needed information
     * @param $technology
     * @return array
     */
    public function getTechnologies(string $technology) : array {
        $collection = array();
        $technologies = $this->getDoctrine()->getRepository(TechnologiesOfficial::class)->getTechnologies($technology);
        // Put the info into an array
        foreach ($technologies as $tech) {
            /** @var TechnologiesOfficial $tech */
            $entity = array(
                'technology' => $tech->getTechnology(),
                'counter' => $tech->getCounter()
            );
            array_push($collection, $entity);
        }
        return $collection;
    }
}
