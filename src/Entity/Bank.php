<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BankRepository")
 */
class Bank {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     * @var $uid int
     */
    private $uid;
    /**
     * @ORM\Column(type="string")
     * @var $type string
     */
    private $type;
    /**
     * @ORM\Column(type="string")
     * @var $payPalAccount string
     */
    private $payPalAccount;
    /**
     * @ORM\Column(type="string")
     * @var $bankName string
     */
    private $bankName;
    /**
     * @ORM\Column(type="string")
     * @var $accountNr string
     */
    private $accountNr;
    /**
     * @ORM\Column(type="integer")
     * @var $accountHolder int
     */
    private $accountHolder;
    /**
     * @ORM\Column(type="string")
     * @var $branchCode string
     */
    private $branchCode;
    /**
     * @ORM\Column(type="string")
     * @var $branchLocation string
     */
    private $branchLocation;

    // Getters and Setters

    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid(int $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getPayPalAccount(): string
    {
        return $this->payPalAccount;
    }

    /**
     * @param string $payPalAccount
     */
    public function setPayPalAccount(string $payPalAccount): void
    {
        $this->payPalAccount = $payPalAccount;
    }

    /**
     * @return string
     */
    public function getBankName(): string
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     */
    public function setBankName(string $bankName): void
    {
        $this->bankName = $bankName;
    }

    /**
     * @return string
     */
    public function getAccountNr(): string
    {
        return $this->accountNr;
    }

    /**
     * @param string $accountNr
     */
    public function setAccountNr(string $accountNr): void
    {
        $this->accountNr = $accountNr;
    }

    /**
     * @return int
     */
    public function getAccountHolder(): int
    {
        return $this->accountHolder;
    }

    /**
     * @param int $accountHolder
     */
    public function setAccountHolder(int $accountHolder): void
    {
        $this->accountHolder = $accountHolder;
    }

    /**
     * @return string
     */
    public function getBranchCode(): string
    {
        return $this->branchCode;
    }

    /**
     * @param string $branchCode
     */
    public function setBranchCode(string $branchCode): void
    {
        $this->branchCode = $branchCode;
    }

    /**
     * @return string
     */
    public function getBranchLocation(): string
    {
        return $this->branchLocation;
    }

    /**
     * @param string $branchLocation
     */
    public function setBranchLocation(string $branchLocation): void
    {
        $this->branchLocation = $branchLocation;
    }


}
