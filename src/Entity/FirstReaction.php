<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FirstReactionRepository")
 */
class FirstReaction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $uid;
    /**
     * @ORM\Column(type="integer")
     */
    private $time;

    /**
     * @return int|null
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getUid(): ?int {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid): void {
        $this->uid = $uid;
    }

    /**
     * @return int|null
     */
    public function getTime(): ?int {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time): void {
        $this->time = $time;
    }

}
