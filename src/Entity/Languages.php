<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LanguagesRepository")
 */
class Languages {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $uid;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $skillId;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $taskId;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $language;

    // Getters and Setters

    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUid() {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid): void {
        $this->uid = $uid;
    }

    /**
     * @return int|null
     */
    public function getSkillId(): ?int {
        return $this->skillId;
    }

    /**
     * @param mixed $skillId
     */
    public function setSkillId($skillId): void {
        $this->skillId = $skillId;
    }

    /**
     * @return int|null
     */
    public function getTaskId(): ?int {
        return $this->taskId;
    }

    /**
     * @param mixed $taskId
     */
    public function setTaskId($taskId): void {
        $this->taskId = $taskId;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language): void {
        $this->language = $language;
    }

}
