<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentRepository")
 */
class Payment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     ** @var $uid int
     */
    private $uid;
    /**
     * @ORM\Column(type="integer", nullable=true)
     ** @var $workId int|null
     */
    private $workId;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $taskId int|null
     */
    private $taskId;
    /**
     * @ORM\Column(name="`before`", type="integer", nullable=true)
     * @var $before int|null
     */
    private $before;
    /**
     * @ORM\Column(name="`after`", type="integer", nullable=true)
     * @var $after int|null
     */
    private $after;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $before_after int|null
     */
    private $before_after;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $after_milestone int|null
     */
    private $after_milestone;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $min int|null
     */
    private $min;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $max int|null
     */
    private $max;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $hourlyRate int|null
     */
    private $hourlyRate;

    // Getters and Setters

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUid(): int {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid(int $uid): void {
        $this->uid = $uid;
    }

    /**
     * @return int|null
     */
    public function getWorkId(): ?int {
        return $this->workId;
    }

    /**
     * @param int|null $workId
     */
    public function setWorkId(?int $workId): void {
        $this->workId = $workId;
    }

    /**
     * @return int|null
     */
    public function getTaskId(): ?int {
        return $this->taskId;
    }

    /**
     * @param int|null $taskId
     */
    public function setTaskId(?int $taskId): void {
        $this->taskId = $taskId;
    }

    /**
     * @return int|null
     */
    public function getBefore(): ?int {
        return $this->before;
    }

    /**
     * @param int|null $before
     */
    public function setBefore(?int $before): void {
        $this->before = $before;
    }

    /**
     * @return int|null
     */
    public function getAfter(): ?int {
        return $this->after;
    }

    /**
     * @param int|null $after
     */
    public function setAfter(?int $after): void {
        $this->after = $after;
    }

    /**
     * @return int|null
     */
    public function getBeforeAfter(): ?int {
        return $this->before_after;
    }

    /**
     * @param int|null $before_after
     */
    public function setBeforeAfter(?int $before_after): void {
        $this->before_after = $before_after;
    }

    /**
     * @return int|null
     */
    public function getAfterMilestone(): ?int {
        return $this->before_after;
    }

    /**
     * @param int|null $after_milestone
     */
    public function setAfterMilestone(?int $after_milestone): void {
        $this->after_milestone = $after_milestone;
    }

    /**
     * @return int|null
     */
    public function getMin(): ?int {
        return $this->min;
    }

    /**
     * @param int|null $min
     */
    public function setMin(?int $min): void {
        $this->min = $min;
    }

    /**
     * @return int|null
     */
    public function getMax(): ?int {
        return $this->max;
    }

    /**
     * @param int|null $max
     */
    public function setMax(?int $max): void {
        $this->max = $max;
    }

    /**
     * @return int|null
     */
    public function getHourlyRate(): ?int {
        return $this->hourlyRate;
    }

    /**
     * @param int|null $hourlyRate
     */
    public function setHourlyRate(?int $hourlyRate): void {
        $this->hourlyRate = $hourlyRate;
    }

   // Getters and Setters











}
