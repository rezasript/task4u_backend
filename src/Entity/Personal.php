<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonalRepository")
 */
class Personal {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $uid;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $firstName;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $lastName;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $country;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $gender;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $dateOfBirth;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $profileImage;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $firstReactionTime;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @return mixed
     */
    public function getRole() {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role): void {
        $this->role = $role;
    }
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $role;

    // Getters and Setters

    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUid(): ?int {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid): void {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getFirstName(): ?string {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName): void {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName(): ?string {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName): void {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getCity(): ?string {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry(): ?string {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getPhone(): ?string {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getGender(): ?string {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender): void {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getDateOfBirth(): ?string {
        return $this->dateOfBirth;
    }

    /**
     * @param mixed $dateOfBirth
     */
    public function setDateOfBirth($dateOfBirth): void {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @return mixed
     */
    public function getProfileImage(): ?string {
        return $this->profileImage;
    }

    /**
     * @param mixed $profileImage
     */
    public function setProfileImage($profileImage): void {
        $this->profileImage = $profileImage;
    }

    /**
     * @return mixed
     */
    public function getFirstReactionTime(): ?int {
        return $this->firstReactionTime;
    }

    /**
     * @param mixed $firstReactionTime
     */
    public function setFirstReactionTime($firstReactionTime): void {
        $this->firstReactionTime = $firstReactionTime;
    }

    /**
     * @return mixed
     */
    public function getDescription(): string {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void {
        $this->description = $description;
    }

}
