<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SkillsRepository")
 */
class Skills {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $uid;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $taskId;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $highestEducation;

    // Getters and Setters
    /**
     * @return int|null
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getUid(): ?int {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid): void {
        $this->uid = $uid;
    }

    /**
     * @return int|null
     */
    public function getTaskId(): ?int {
        return $this->taskId;
    }

    /**
     * @param mixed $taskId
     */
    public function setTaskId($taskId): void {
        $this->taskId = $taskId;
    }

    /**
     * @return string|null
     */
    public function getHighestEducation(): ?string {
        return $this->highestEducation;
    }

    /**
     * @param mixed $highestEducation
     */
    public function setHighestEducation($highestEducation): void {
        $this->highestEducation = $highestEducation;
    }

}
