<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 */
class Task
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     * @var $from int
     */
    private $from;
    /**
     * @ORM\Column(type="integer")
     * @var $to int
     */
    private $to;
    /**
     * @ORM\Column(type="integer")
     * @var $timestamp int
     */
    private $timestamp;
    /**
     * @ORM\Column(type="string")
     * @var $title string
     */
    private $title;
    /**
     * @ORM\Column(type="string")
     * @var $description string
     */
    private $description;
    /**
     * @ORM\Column(type="boolean")
     * @var $milestones bool
     */
    private $milestones;
    /**
     * @ORM\Column(type="integer")
     * @var $start int
     */
    private $start;
    /**
     * @ORM\Column(type="integer")
     * @var $end int
     */
    private $end;
    /**
     * @ORM\Column(type="string")
     * @var $gender string
     */
    private $gender;
    /**
     * @ORM\Column(type="string")
     * @var $location string
     */
    private $location;
    /**
     * @ORM\Column(type="integer")
     * @var $age int
     */
    private $age;
    /**
     * @ORM\Column(type="integer")
     * @var $workload int
     */
    private $workload;
    /**
     * @ORM\Column(type="integer")
     * @var $firstReactionTime int
     */
    private $firstReactionTime;

    // Getters and Setters

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getFrom(): int
    {
        return $this->from;
    }

    /**
     * @param int $from
     */
    public function setFrom(int $from): void
    {
        $this->from = $from;
    }

    /**
     * @return int
     */
    public function getTo(): int
    {
        return $this->to;
    }

    /**
     * @param int $to
     */
    public function setTo(int $to): void
    {
        $this->to = $to;
    }

    /**
     * @return int
     */
    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    /**
     * @param int $timestamp
     */
    public function setTimestamp(int $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isMilestones(): bool
    {
        return $this->milestones;
    }

    /**
     * @param bool $milestones
     */
    public function setMilestones(bool $milestones): void
    {
        $this->milestones = $milestones;
    }

    /**
     * @return int
     */
    public function getStart(): int
    {
        return $this->start;
    }

    /**
     * @param int $start
     */
    public function setStart(int $start): void
    {
        $this->start = $start;
    }

    /**
     * @return int
     */
    public function getEnd(): int
    {
        return $this->end;
    }

    /**
     * @param int $end
     */
    public function setEnd(int $end): void
    {
        $this->end = $end;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender(string $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation(string $location): void
    {
        $this->location = $location;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge(int $age): void
    {
        $this->age = $age;
    }

    /**
     * @return int
     */
    public function getWorkload(): int
    {
        return $this->workload;
    }

    /**
     * @param int $workload
     */
    public function setWorkload(int $workload): void
    {
        $this->workload = $workload;
    }

    /**
     * @return int
     */
    public function getFirstReactionTime(): int
    {
        return $this->firstReactionTime;
    }

    /**
     * @param int $firstReactionTime
     */
    public function setFirstReactionTime(int $firstReactionTime): void
    {
        $this->firstReactionTime = $firstReactionTime;
    }

}
