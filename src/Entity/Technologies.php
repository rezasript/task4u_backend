<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TechnologiesRepository")
 */
class Technologies
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $uid;
    /**
     * @ORM\Column(type="integer")
     */
    private $skillId;
    /**
     * @ORM\Column(type="string")
     */
    private $tech;
    /**
     * @ORM\Column(type="string")
     */
    private $vote;

    // Getters and Setters

    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUid() {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid): void {
        $this->uid = $uid;
    }

    /**
     * @return int|null
     */
    public function getSkillId(): ?int {
        return $this->skillId;
    }

    /**
     * @param mixed $skillId
     */
    public function setSkillId($skillId): void {
        $this->skillId = $skillId;
    }

    /**
     * @return string|null
     */
    public function getTech(): ?string {
        return $this->tech;
    }

    /**
     * @param mixed $tech
     */
    public function setTech($tech): void {
        $this->tech = $tech;
    }

    /**
     * @return mixed
     */
    public function getVote() {
        return $this->vote;
    }

    /**
     * @param mixed $vote
     */
    public function setVote($vote): void {
        $this->vote = $vote;
    }

}
