<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TechnologiesOfficialRepository")
 */
class TechnologiesOfficial
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $technology;

    /**
     * @ORM\Column(type="integer")
     */
    private $counter;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $approved;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTechnology(): ?string
    {
        return $this->technology;
    }

    public function setTechnology(string $technology): self
    {
        $this->technology = $technology;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCounter(): int {
        return $this->counter;
    }

    /**
     * @param mixed $counter
     * @return TechnologiesOfficial
     */
    public function setCounter(int $counter): self {
        $this->counter = $counter;
        return $this;
    }

    public function getApproved(): ?bool
    {
        return $this->approved;
    }

    public function setApproved(?bool $approved): self
    {
        $this->approved = $approved;

        return $this;
    }
}
