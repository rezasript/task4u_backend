<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeOfTaskRepository")
 */
class TypeOfTask
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $uid int
     */
    private $uid;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $workId int
     */
    private $workId;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $taskId int
     */
    private $taskId;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $project int
     */
    private $project;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $feature int
     */
    private $feature;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $bugfix int
     */
    private $bugfix;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var $other int
     */
    private $other;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @var $descriptionOfOther string
     */
    private $descriptionOfOther;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUid(): int {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid(int $uid): void {
        $this->uid = $uid;
    }

    /**
     * @return int
     */
    public function getWorkId(): int {
        return $this->workId;
    }

    /**
     * @param int $workId
     */
    public function setWorkId(int $workId): void {
        $this->workId = $workId;
    }

    /**
     * @return int
     */
    public function getTaskId(): int {
        return $this->taskId;
    }

    /**
     * @param int $taskId
     */
    public function setTaskId(int $taskId): void {
        $this->taskId = $taskId;
    }

    /**
     * @return int
     */
    public function getProject(): int {
        return $this->project;
    }

    /**
     * @param int $project
     */
    public function setProject(int $project): void {
        $this->project = $project;
    }

    /**
     * @return int
     */
    public function getFeature(): int {
        return $this->feature;
    }

    /**
     * @param int $feature
     */
    public function setFeature(int $feature): void {
        $this->feature = $feature;
    }

    /**
     * @return int
     */
    public function getBugfix(): int {
        return $this->bugfix;
    }

    /**
     * @param int $bugfix
     */
    public function setBugfix(int $bugfix): void {
        $this->bugfix = $bugfix;
    }

    /**
     * @return int
     */
    public function getOther(): int {
        return $this->other;
    }

    /**
     * @param int $other
     */
    public function setOther(int $other): void {
        $this->other = $other;
    }

    /**
     * @return string
     */
    public function getDescriptionOfOther(): string {
        return $this->descriptionOfOther;
    }

    /**
     * @param string $descriptionOfOther
     */
    public function setDescriptionOfOther(string $descriptionOfOther): void {
        $this->descriptionOfOther = $descriptionOfOther;
    }

}
