<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WorkRepository")
 */
class Work {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $uid;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $workload;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $milestones;

    // Getters and Setters
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUid(): int {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid): void {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getWorkload(): ?int {
        return $this->workload;
    }

    /**
     * @param mixed $workload
     */
    public function setWorkload($workload): void {
        $this->workload = $workload;
    }

    /**
     * @return mixed
     */
    public function getMilestones(): ?int {
        return $this->milestones;
    }

    /**
     * @param mixed $milestones
     */
    public function setMilestones($milestones): void {
        $this->milestones = $milestones;
    }

}
