<?php
namespace App\Helper;

/**
 * A helper class that implements useful methods that will help your code
 * Class Helper
 * @package App\Helper
 */
class Helper {
    /**
     * Checks if the request matches with the database columns
     * @param $request
     * @return bool
     */
    public function keyAccess(array $request) {
        $allowedKeys = array('title', 'description', 'phone', 'email', 'first_name', 'last_name', 'paymentTime', 'hourlyRate', 'dateOfBirth', 'workload', 'before', 'after', 'before_after', 'after_milestone', 'minPrice', 'maxPrice', 'deadlineFrom', 'deadlineUntil', 'city', 'country', 'gender', 'highestEducation', 'project', 'feature', 'bugfix', 'other', 'language', 'tech', 'milestones');
        $keyAccess = true;
        foreach ($request as $key => $value) {
            if (!in_array($key, $allowedKeys)) {
                $keyAccess = false;
                break;
            }
        }
        return $keyAccess;
    }

    /**
     * Gets the table name by column
     * @param string $column
     * @return string
     */
    public function getTableName(string $column) {
        switch ($column) {
            case 'gender':
            case 'firstName':
            case 'lastName':
            case 'city':
            case 'country':
            case 'phone':
            case 'dateOfBirth':
            case 'profileImage':
                return 'Personal';
            case 'language':
                return 'Languages';
            case 'highestEducation':
                return 'Skills';
            case 'tech':
            case 'vote':
                return 'technologies';
            case 'project':
            case 'feature':
            case 'bugfix':
            case 'other':
            case 'descriptionOfOther':
                return 'TypeOfTask';
            case 'workload':
            case 'milestones':
                return 'Work';
            case 'hourlyRate':
            case 'before':
            case 'after':
            case 'before_after':
            case 'after_milestone':
                return 'Payment';
        }
    }
}
