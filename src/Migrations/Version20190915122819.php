<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190915122819 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE languages CHANGE skill_id skill_id INT DEFAULT NULL, CHANGE task_id task_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment CHANGE work_id work_id INT DEFAULT NULL, CHANGE task_id task_id INT DEFAULT NULL, CHANGE `before` `before` TINYINT(1) DEFAULT NULL, CHANGE after after TINYINT(1) DEFAULT NULL, CHANGE before_after before_after TINYINT(1) DEFAULT NULL, CHANGE milestone milestone TINYINT(1) DEFAULT NULL, CHANGE min min INT DEFAULT NULL, CHANGE max max INT DEFAULT NULL, CHANGE hourly_rate hourly_rate INT DEFAULT NULL');
        $this->addSql('ALTER TABLE personal CHANGE first_name first_name VARCHAR(255) DEFAULT NULL, CHANGE last_name last_name VARCHAR(255) DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT NULL, CHANGE country country VARCHAR(255) DEFAULT NULL, CHANGE phone phone VARCHAR(255) DEFAULT NULL, CHANGE gender gender VARCHAR(255) DEFAULT NULL, CHANGE date_of_birth date_of_birth VARCHAR(255) DEFAULT NULL, CHANGE profile_image profile_image VARCHAR(255) DEFAULT NULL, CHANGE first_reaction_time first_reaction_time INT DEFAULT NULL');
        $this->addSql('ALTER TABLE skills CHANGE task_id task_id INT DEFAULT NULL, CHANGE highest_education highest_education VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE work CHANGE workload workload INT DEFAULT NULL, CHANGE hourly_rate hourly_rate INT DEFAULT NULL, CHANGE milestones milestones TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE languages CHANGE skill_id skill_id INT DEFAULT NULL, CHANGE task_id task_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment CHANGE work_id work_id INT NOT NULL, CHANGE task_id task_id INT NOT NULL, CHANGE `before` `before` TINYINT(1) NOT NULL, CHANGE after after TINYINT(1) NOT NULL, CHANGE before_after before_after TINYINT(1) NOT NULL, CHANGE milestone milestone TINYINT(1) NOT NULL, CHANGE min min INT NOT NULL, CHANGE max max INT NOT NULL, CHANGE hourly_rate hourly_rate INT NOT NULL');
        $this->addSql('ALTER TABLE personal CHANGE first_name first_name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE last_name last_name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE city city VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE country country VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE phone phone VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE gender gender VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE date_of_birth date_of_birth VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE profile_image profile_image VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE first_reaction_time first_reaction_time INT DEFAULT NULL');
        $this->addSql('ALTER TABLE skills CHANGE task_id task_id INT DEFAULT NULL, CHANGE highest_education highest_education VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE work CHANGE workload workload INT DEFAULT NULL, CHANGE hourly_rate hourly_rate INT DEFAULT NULL, CHANGE milestones milestones TINYINT(1) DEFAULT \'NULL\'');
    }
}
