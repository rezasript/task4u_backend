<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191020124544 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE technologies_official (id INT AUTO_INCREMENT NOT NULL, technology VARCHAR(50) NOT NULL, approved TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE form_inputs CHANGE data data JSON NOT NULL');
        $this->addSql('ALTER TABLE languages CHANGE skill_id skill_id INT DEFAULT NULL, CHANGE task_id task_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment CHANGE work_id work_id INT DEFAULT NULL, CHANGE task_id task_id INT DEFAULT NULL, CHANGE `before` `before` INT DEFAULT NULL, CHANGE after `after` INT DEFAULT NULL, CHANGE before_after before_after INT DEFAULT NULL, CHANGE min min INT DEFAULT NULL, CHANGE max max INT DEFAULT NULL, CHANGE hourly_rate hourly_rate INT DEFAULT NULL');
        $this->addSql('ALTER TABLE personal CHANGE first_name first_name VARCHAR(255) DEFAULT NULL, CHANGE last_name last_name VARCHAR(255) DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT NULL, CHANGE country country VARCHAR(255) DEFAULT NULL, CHANGE phone phone VARCHAR(255) DEFAULT NULL, CHANGE gender gender VARCHAR(255) DEFAULT NULL, CHANGE date_of_birth date_of_birth VARCHAR(255) DEFAULT NULL, CHANGE profile_image profile_image VARCHAR(255) DEFAULT NULL, CHANGE first_reaction_time first_reaction_time INT DEFAULT NULL');
        $this->addSql('ALTER TABLE skills CHANGE task_id task_id INT DEFAULT NULL, CHANGE highest_education highest_education VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE work CHANGE workload workload INT DEFAULT NULL, CHANGE milestones milestones TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE technologies_official');
        $this->addSql('ALTER TABLE form_inputs CHANGE data data LONGTEXT NOT NULL COLLATE utf8mb4_bin');
        $this->addSql('ALTER TABLE languages CHANGE skill_id skill_id INT DEFAULT NULL, CHANGE task_id task_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment CHANGE work_id work_id INT DEFAULT NULL, CHANGE task_id task_id INT DEFAULT NULL, CHANGE `before` `before` INT DEFAULT NULL, CHANGE `after` after INT DEFAULT NULL, CHANGE before_after before_after INT DEFAULT NULL, CHANGE min min INT DEFAULT NULL, CHANGE max max INT DEFAULT NULL, CHANGE hourly_rate hourly_rate INT DEFAULT NULL');
        $this->addSql('ALTER TABLE personal CHANGE first_name first_name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE last_name last_name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE city city VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE country country VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE phone phone VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE gender gender VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE date_of_birth date_of_birth VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE profile_image profile_image VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE first_reaction_time first_reaction_time INT DEFAULT NULL');
        $this->addSql('ALTER TABLE skills CHANGE task_id task_id INT DEFAULT NULL, CHANGE highest_education highest_education VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE work CHANGE workload workload INT DEFAULT NULL, CHANGE milestones milestones TINYINT(1) DEFAULT \'NULL\'');
    }
}
