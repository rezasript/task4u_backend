<?php

namespace App\Repository;

use App\Entity\FirstReaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FirstReaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method FirstReaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method FirstReaction[]    findAll()
 * @method FirstReaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FirstReactionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FirstReaction::class);
    }

    // /**
    //  * @return FirstReaction[] Returns an array of FirstReaction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FirstReaction
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
