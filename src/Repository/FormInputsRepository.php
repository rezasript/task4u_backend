<?php

namespace App\Repository;

use App\Entity\FormInputs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FormInputs|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormInputs|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormInputs[]    findAll()
 * @method FormInputs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormInputsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormInputs::class);
    }

    // /**
    //  * @return FormInputs[] Returns an array of FormInputs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FormInputs
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
