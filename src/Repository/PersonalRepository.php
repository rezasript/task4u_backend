<?php
namespace App\Repository;

use App\Entity\Personal;
use App\Helper\Helper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Exception;

/**
 * @method Personal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Personal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Personal[]    findAll()
 * @method Personal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonalRepository extends ServiceEntityRepository {
    private $helper;

    public function __construct(ManagerRegistry $registry, Helper $helper) {
        parent::__construct($registry, Personal::class);
        $this->helper = $helper;
    }

    /**
     * @param $select
     * @param $where
     * @param $value
     * @return Personal[] Returns an array of Personal objects
     */
    public function data($select, $where, $value) {
        return $this->createQueryBuilder('p')
            ->select($select)
            ->where('p.' . $where  . ' = :where')
            ->setParameter('where', $value)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param string $year
     * @return array
     * @throws Exception
     */
    public function dateOfBirth(string $year): array {
        // get the DateTimes
        $startDate = new \DateTime("midnight January 1, $year");
        $year += 1;
        $endDate = new \DateTime("midnight January 1, $year");
        // get the timestamps
        $start = $startDate->format('U');
        $end = $endDate->format('U');

        return $this->createQueryBuilder('p')
            ->select('p.dateOfBirth')
            ->where("p.dateOfBirth > :start")
            ->andWhere("p.dateOfBirth < :end")
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $select
     * @param $where
     * @param $value
     * @param array $optionalColumns
     * @param array $optionalValues
     * @return QueryBuilder|mixed
     * @throws Exception
     */
    public function matchWithMandatoryEntities($select, $where, $value, array $optionalColumns, array $optionalValues) {
        $query = null;
        if (!empty($optionalColumns)) {
            $query = $this->createQueryBuilder('p')->select($select);

            // Get the columns
            $tableNames = [];
            for ($i = 0;  $i < count($optionalColumns); $i++) {
                $join = $this->helper->getTableName(array_values($optionalColumns)[$i]);
                if (!in_array($join, $tableNames)) {
                    $query = $query->innerJoin("App\Entity\\$join", $join);
                    array_push($tableNames, $join);
                }
            }

            $query = $query->where('p.' . $where  . ' = :where');

            for ($i = 0;  $i < count($optionalColumns); $i++) {
                $prefix = $this->helper->getTableName(array_values($optionalColumns)[$i]);
                $query = $query->andWhere("$prefix.uid = :uid$i")->setParameter("uid$i", $value); // UID
                // if the column is age
                if ($optionalColumns[$i] === 'dateOfBirth') {
                    $inputAge = date('Y') - $optionalValues[$i];
                    $query = $query->andWhere("p.dateOfBirth = :dateOfBirth")->setParameter('dateOfBirth', $this->dateOfBirth($inputAge));
                }
                else {
                    $query->andWhere("$prefix.$optionalColumns[$i] = :column$i")->setParameter("column$i", $optionalValues[$i]); // Column
                }
            }

            $query = $query
                ->setParameter('where', $value)
                ->getQuery()
                ->getResult()
                ;
        }
        return $query;
    }

    /**
     * Get a column by ID
     * @param $table
     * @param $select
     * @param $where
     * @param $value
     * @param $uid
     * @return mixed
     */
    public function columnByUid($table, $select, $where, $value, $uid) {
        $prefix = strtolower($table);

        if ($table != 'Personal') {
            return $this->createQueryBuilder('p')
                ->select("p.uid")
                ->innerJoin("App\Entity\\$table", $prefix)
                ->where("$prefix.uid  = :uid")
                ->andWhere("$prefix.$where = :value")
                ->setParameter('uid', $uid)
                ->setParameter('value', $value)
                ->getQuery()
                ->getResult();
        }
        else {
            return $this->createQueryBuilder('p')
                ->select("p.$select")
                ->where('p.uid  = :uid')
                ->andWhere("p.$where = :value")
                ->setParameter('uid', $uid)
                ->setParameter('value', $value)
                ->getQuery()
                ->getResult();
        }
    }

    /**
     * @param $select
     * @return mixed
     */
    public function getWholeColumn($select) {
        return $this->createQueryBuilder('p')
            ->select($select)
            ->getQuery()
            ->getResult()
            ;
    }


    /*
    public function findOneBySomeField($value): ?Personal
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
