<?php

namespace App\Repository;

use App\Entity\TechnologiesOfficial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method TechnologiesOfficial|null find($id, $lockMode = null, $lockVersion = null)
 * @method TechnologiesOfficial|null findOneBy(array $criteria, array $orderBy = null)
 * @method TechnologiesOfficial[]    findAll()
 * @method TechnologiesOfficial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TechnologiesOfficialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TechnologiesOfficial::class);
    }

    public function getTechnologies($technology) {
        return $this->createQueryBuilder('t')
            ->where('t.technology LIKE :technology')
            ->setParameter('technology', "%$technology%")
            ->setMaxResults(6)
            ->orderBy('t.counter', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Update Updates the counter
     * @param $technology
     * @param $counter
     * @return QueryBuilder
     */
    public function updateCounter($technology, $counter) {
        return $this->createQueryBuilder('t')
            ->set('t.technology', $technology)
            ->set('t.counter', $counter)
            ->where('t.technology = :where')
            ->setParameter('technology', $technology)
        ;
    }

    // /**
    //  * @return TechnologiesOfficial[] Returns an array of TechnologiesOfficial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TechnologiesOfficial
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
