<?php

namespace App\Repository;

use App\Entity\TypeOfTask;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TypeOfTask|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeOfTask|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeOfTask[]    findAll()
 * @method TypeOfTask[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeOfTaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeOfTask::class);
    }

    /**
     * @param $select
     * @param $where
     * @param $value
     * @return mixed
     */
    public function data($select, $where, $value) {
        return $this->createQueryBuilder('t')
            ->select($select)
            ->where('t.' . $where  . ' = :where')
            ->setParameter('where', $value)
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return TypeOfTask[] Returns an array of TypeOfTask objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeOfTask
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
