<?php

namespace App\Repository;

use App\Entity\Users;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Users|null find($id, $lockMode = null, $lockVersion = null)
 * @method Users|null findOneBy(array $criteria, array $orderBy = null)
 * @method Users[]    findAll()
 * @method Users[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Users::class);
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getHash($email) {
        return $this->createQueryBuilder('u')
            ->select('u.password')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getResult()
         ;
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getId($email) {
        return $this->createQueryBuilder('u')
            ->select('u.id')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param $data
     * @param $hashedPassword
     * @return Users[] Returns an array of Users objects
     */
    public function getLoginAuth($data, $hashedPassword) {
        if (password_verify($data['password'], $hashedPassword)) {
            return $this->createQueryBuilder('u')
                ->select('u.email, u.password')
                ->where('u.email = :email')
                ->andWhere('u.password = :password')
                ->setParameter('email', $data['email'])
                ->setParameter('password', $hashedPassword)
                ->setMaxResults(10)
                ->getQuery()
                ->getResult()
                ;
        }
        else {
            return null;
        }
    }


    /*
    public function findOneBySomeField($value): ?Users
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
