<?php
namespace App\Service;

use App\Dao\AutofillDao;

/**
 * Class AutofillService
 * @package App\Service
 */
class AutofillService {
    private $autofillDao;

    public function __construct(AutofillDao $autofillDao) {
        $this->autofillDao = $autofillDao;
    }

    /**
     * @param $data
     */
    public function save($data) : void {
        $this->autofillDao->save($data);
    }

    /**
     * Returns true, if enough data is available
     * @param int $rows
     * @return bool
     */
    public function hasEnoughData(int $rows = 3) : bool {
        $hasData = $this->autofillDao->getData();
        if (count($hasData) >= $rows) {
            return true;
        }
        return false;
    }

    /**
     * Will return a JSON with the most used entities
     * @param int $minimumProbability
     * @return array|null
     */
    public function getData(int $minimumProbability = 10) : ?array {
        if ($this->hasEnoughData(1)) {
            // Add up the keys
            $keyList = array();
            foreach($this->autofillDao->getData() as $data) {
                // Get JSON
                $columns = $data->getData();
                $keys = array_keys($columns['request']);
                foreach($keys as $key) {
                    // Add the key, since it does not exist
                    if (!array_key_exists($key, $keyList)) {
                        $keyList[$key] = array();
                    }
                    // The key has an array
                    if (is_array($columns['request'][$key])) {
                        foreach($columns['request'][$key] as $subKey) {
                            array_push($keyList[$key], strtolower($subKey));
                        }
                    }
                    // It's a string
                    else {
                        array_push($keyList[$key], strtolower($columns['request'][$key]));
                    }
                }
            }

            // Count the values and pick the most used ones
            $result = array();
            $minimumProbabilityCounter = array();
            foreach($keyList as $key => $value) {
                $count = array_count_values($value);

                foreach ($count as $itemNumber) {
                    // Get the value by the minimum probability of a percentage
                    $algorithm = $itemNumber / count($this->autofillDao->getData()) * 100;

                    if ($algorithm >= $minimumProbability) {
                        array_push($minimumProbabilityCounter, $algorithm);
                        arsort($count);
                        $result[$key] = key($count);
                    }
                }
            }

            // Store the result
            $endResult['result'] = $result;
            $endResult['probability'] = (count($minimumProbabilityCounter) > 0) ? array_sum($minimumProbabilityCounter) / count($minimumProbabilityCounter) : 0;
        }
        $endResult = $endResult ?? null;
        return $endResult;
    }
}
