<?php
namespace App\Service;

use App\Dao\LoginDao;
use App\Entity\Cookies;
use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LoginService extends AbstractController {
    private $loginDao;

    // Constructor
    /**
     * LoginService constructor.
     * @param LoginDao $loginDao
     */
    function __construct(LoginDao $loginDao) {
        $this->loginDao = $loginDao;
    }

    // Login

    /**
     * @param $data
     * @return Users[]|int
     */
    public function login($data) {
        $isAuth = $this->loginDao->login($data);
        if ($isAuth) {
            // Set the sessions
            $this->get('session')->set('user', $data['email']); // E-Mail
            $this->get('session')->set('id', $this->loginDao->getId($data['email'])); // ID
            $this->loginDao->setCookie();
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @description: Checks if the user is logged in by a cookie
     * @param string $key
     * @return string
     */
    public function getCookie(string $key): string {
        // Forget cookie, because session is set
        if ($this->get('session')->get('email')) {
            return 1;
        }
        // Go ahead with the cookie process
        $entity = $this->getDoctrine()->getRepository(Cookies::class)->findOneBy(array('login' => $key));
        if ($entity) {
            return $entity->getLogin();
        }
        return 0;
    }

    /**
     * @description Get user's cookie by a UID
     */
    public function getCookieByUid() {
        if ($this->get('session')->get('id')) {
            $cookie = $this->getDoctrine()->getRepository(Cookies::class)->findOneBy(array('uid' => $this->get('session')->get('id')));
            if ($cookie) {
                return $cookie->getLogin();
            }
            return 0;
        }
        return 0;
    }
}
