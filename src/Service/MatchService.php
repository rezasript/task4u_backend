<?php
namespace App\Service;

use App\Algorithm\Calculator;
use App\Algorithm\Collection;
use App\Dao\MatchDao;
use App\Helper\Helper;

class MatchService {
    public $users;
    private $collection;
    private $calculator;
    private $matchDao;
    private $helper;
    private $autoService;

    function __construct(Collection $collection, Calculator $calculator, MatchDao $matchDao, Helper $helper, AutofillService $autofillService) {
        $this->collection = $collection;
        $this->calculator = $calculator;
        $this->matchDao = $matchDao;
        $this->helper = $helper;
        $this->autoService = $autofillService;
    }

    public function getMatch($data) {
        $result = 'No match  (' . __DIR__ . ':#' . __LINE__ . ')';
        $request = $data['request'];

        // Let's see if the user is allowed
        $keyAccess = $this->helper->keyAccess($request);

        // User is allowed. Let's go on
        if ($keyAccess) {
            // Get all users, if they exist
            if (!empty($this->collection->getAllUsers($request))) {
                $this->users = $this->collection->getAllUsers($request);

                // Filter users by mandatory entities
                if (!empty($data['important']['columns']) && !empty($data['important']['values'])) {
                    try {
                        $this->users = $this->matchDao->filterUsers($this->users, $data['important']['columns'], $data['important']['values']);
                    } catch (\Exception $e) {
                        var_dump($e->getMessage());
                    }
                }

                // Get the result, if it exists
                if (!empty($this->calculator->result($request, $this->users))) {
                    // Get the data
                    $result = $this->matchDao->getDeveloperInfo($this->calculator->result($request, $this->users), $request);
                    $this->autoService->save($data);
                }
            }
        }
        return $result;
    }

}
