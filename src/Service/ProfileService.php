<?php
namespace App\Service;

use App\Dao\ProfileDao;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class ProfileService extends AbstractController {
    private $profileDao;

    /**
     * ProfileService constructor.
     * @param ProfileDao $profileDao
     */
    function __construct(ProfileDao $profileDao) {
        $this->profileDao = $profileDao;
    }

    /**
     * @param $data
     * @return int
     */
    public function update($data) {
        $result = 0;
        $table = $data['table'] ?? null;
        $column = $data['column'] ?? null;
        $value = $data['value'] ?? null;

        /*
         * Table: personal
         * Column: firstName|lastName|city|country
         * Value
         */

        // Update the columns which are inside $columns
        if ($table == 'personal') {
            $columns = array('firstName', 'lastName', 'city', 'country', 'description', 'role');
            if (in_array($column, $columns)) {
                if (strlen($value) < 40 || in_array('description', $columns)) {
                    $this->profileDao->updatePersonal($column, $value);
                    $result = 1;
                }
            } // Update the column phone
            elseif ($column == 'phone') {
                if (preg_match('/^[()0-9+-? ]+$/', $value) == 1) {
                    $this->profileDao->updatePersonal($column, $value);
                    $result = 1;
                }
            } // Update the column gender
            elseif ($column == 'gender') {
                if (strlen($value) < 2) {
                    $this->profileDao->updatePersonal($column, $value);
                    $result = 1;
                }
            } // Update the column date_of_birth
            elseif ($column == 'dateOfBirth') {
                if (strlen($value) < 20 && is_numeric($value)) {
                    $this->profileDao->updatePersonal($column, $value);
                    $result = 1;
                }
            }

            // Update the column profile_image
            // Todo: That needs to be done
            elseif ($column == 'profileImage') {
                $this->profileDao->updatePersonal($column, $value);
                $result = 1;
            }
        }

        /* Table: typeOfTask
           Column: project|feature|bugfix|other
           Value
        */
        if (($table == 'typeOfTask') && ($column == 'project' || $column == 'bugfix' || $column == 'feature' || $column == 'other')) {
            $value = ($value === true) ? 1: 0;
            return ($this->profileDao->updateTypeOfTask($column, $value) == 1) ? 1 : 0;
        }

        /* Table: languages
           Column: language
           Type: skill|task
           Value
        */
        elseif ($table == 'languages' && $column == 'language' && isset($data['type'])) {
            return $this->profileDao->updateLanguage($data['type'], $value);
        }

        /* Table: payment
           Column: hourly_rate
           Type: skill|task
        */
        elseif ($table == 'payment') {
            $result = 0;
            $columns = ['hourlyRate', 'payday'];
            if (in_array($column, $columns)) {
                $result = 1;
                // Validate the hourly rate
                if ($column == 'hourlyRate' && !is_numeric($value)) {
                    $result = 0;
                }
            }

            // Add to database
            if ($result == 1) {
                $result = ($this->profileDao->updatePayment($data) == 1) ? 1: 0;
            }
        }

        /* Table: skills
           Column: highest_education
           Expected values: 'High school graduate', 'Associate degree', 'Bachelor's degree', 'Master's degree', 'Doctoral degree', 'Professorship'
        */
        if ($table == 'skills' && $column == 'highestEducation') {
            $values = array('High school graduate', 'Associate degree', 'Bachelor\'s degree', 'Master\'s degree', 'Doctoral degree', 'Professorship');
            if (in_array($value, $values)) {
                $result = $this->profileDao->updateSkill($data);
            }
        }

        /* Table: technologies
           Column: tech
           Value
        */
        if ($table == 'technologies') {
            if ($column == 'tech') {
                $technologies = explode(',', $value);
                foreach ($technologies as $technology) {
                    $this->profileDao->updateTech($technology, $technologies);
                }
                $result = 1;
            }
        }

        /* Table: work
           Column: workload|milestones
           Value
        */
        if ($table == 'work') {
            $result = 0;

            // Workload && milestones
            if (($column == 'workload' && $value <= 200 && $value > 0) || (($column == 'milestones') && ($value == 1 || $value == 0))) { // Validate workload and milestones
                $result = $this->profileDao->updateWork($data);
            }
        }

        return $result;
    }

    /**
     * Outputs all data
     * @return array
     */
    public function output(): array {
        return $this->profileDao->output();
    }

    /**
     * Uploads an image file which is less than 5 MB
     * @param Request $request
     * @return array|int
     */
    public function setImage(Request $request) {
        $file = $request->files->get('profileImage');
        $mimeTypes = array('image/jpeg', 'image/png', 'image/gif');
        if (in_array($file->getMimeType(), $mimeTypes) && ($file->getSize() <= 5000000)) {
            $uploadDir = $this->getParameter('uploads_directory') . '/profile-images';
            $filename = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($uploadDir, $filename);
            $this->profileDao->setImage($filename);
            return array('filename' => "$filename");
        }
        return 0;
    }

}
