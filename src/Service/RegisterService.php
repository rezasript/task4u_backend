<?php
namespace App\Service;

use App\Dao\RegisterDao;

class RegisterService {
    private $registerDao;

    function __construct(RegisterDao $registerDao) {
        $this->registerDao= $registerDao;
    }

    // Register
    function register($data) {
        $email    = $data['email'] ?? null;
        $password = $data['password'] ?? null;
        $val_email = false;
        $val_password = false;

        // Email validation
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $val_email = true;
        }
        // Password validation
        if (strlen($password) > 5) {
            $val_password = true;
        }

        // Save data
        if ($val_email && $val_password) {
            $this->registerDao->register($data);
            $validatedData = array('email' => $email, 'password' => $password);
            return json_encode($validatedData);
        }
        // Send error
        else {
            $email    = ($val_email) ? $email : 'error';
            $password = ($val_password) ? $password : 'error';
            $validatedData = array('email' => $email, 'password' => $password);
            return json_encode($validatedData);
        }
    }
}