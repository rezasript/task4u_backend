<?php
namespace App\Service;

use App\Dao\TaskDao;
use App\Helper\Helper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TaskService extends AbstractController {
    private $helper;
    private $receiver;
    private $mailer;
    private $dao;
    private $sender;
    private $request;

    public function __construct(Helper $helper, \Swift_Mailer $mailer, TaskDao $dao) {
        $this->helper = $helper;
        $this->mailer = $mailer;
        $this->dao = $dao;
    }

    /**
     * Validates and sends an email
     * @param array $data Data that should be sent
     * @return int
     */
    public function validateToSendEmail(array $data) {
        if ($this->helper->keyAccess($data['request'])) {
            $id = $this->get('session')->get('id');
            $this->sender = $this->dao->getUserData($id);
            if ($this->sender === 0) {
                return 0;
            }
            $this->request = $data['request'];
            // Uncomment the line below only if you want to test the email in your browser
            //echo $this->renderView('emails/task.twig', ['sender' => $this->sender, 'request' => $this->request]);
            $this->receiver = $this->dao->getReceiversEmail($data['uid']);
            $this->email();
            return 1;
        }
        return 0;
    }

    /**
     * Send an email
     * @return void
     */
    private function email() {
        $message = (new \Swift_Message('Tapalle: You have a message'))
            ->setFrom('admin@tapalle.com')
            ->setTo($this->receiver)
            ->setBody(
                $this->renderView(
                    'emails/task.twig',
                    ['sender' => $this->sender, 'request' => $this->request]
                ),
                'text/html'
            )
        ;

        $this->mailer->send($message);
        //return $this->render('emails/task.twig');
    }
}
