<?php
namespace App\Service;

use App\Dao\TechnologyDao;

class TechnologyService {
    private $technologyDao;

    public function __construct(TechnologyDao $technologyDao) {
        $this->technologyDao = $technologyDao;
    }

    /**
     * Get the needed technologies
     * @param $technology
     * @return array
     */
    public function getTechnologies(string $technology) : array {
        if (is_string($technology)) {
            return $this->technologyDao->getTechnologies($technology);
        }
        return array();
    }
}
